//Slick Slider Function
jQuery('.event-slider').slick({
  slidesToShow: 3,
  slidesToScroll: 1,
  autoplaySpeed: 2500,
  infinite: false,
  arrows: true,
  prevArrow: ' <div class="swiper-btn swiper-btn-prev"><i class="fal fa-chevron-left"></i></div>',
  nextArrow: '<div class="swiper-btn swiper-btn-next"><i class="fal fa-chevron-right"></i></div>',
  dots: false,
  pauseOnHover: true,
  responsive:[
    {
      breakpoint: 1200,
      settings: {
        slidesToShow: 3,
        infinite: true,
        dots: false,
      }
    },

  {
    breakpoint: 1000,
    settings: {
      arrows: false,
      slidesToShow: 1,
      slidesToScroll: 1,
      centerMode: true,
      dots: true,
    }
  },
  {
    breakpoint: 500,
    settings: {
      arrows: false,
      slidesToShow: 1,
      slidesToScroll: 1,
      centerMode: true,
      dots: true,
    }
  }
  ]
});

//IE specific conditional function

if(/MSIE \d|Trident.*rv:/.test(navigator.userAgent))
  {
    $('.solutions-card-thumb, .deliver-prescriptive-action-grid-media-image').each(function()
    {
     var src = jQuery(this).find("img").attr('src');
     if(src)
     {
      jQuery(this).css('background-image', 'url("' + src + '")');
     }
    });
          let $formInput = $('input, textarea, select');
          $formInput.on('focus', function () {
              let that = $(this);
              that.parent().addClass('is-focus');
          });
          $formInput.on('blur', function () {
              let that = $(this);
              if (that.is('input') || that.is('textarea')) {
                  if (that.val().length === 0) {
                      that.parent().removeClass('is-focus');
                  } else {
                      that.parent().addClass('is-filled');
                      that.parent().removeClass('is-focus');
                  }
              } else {
                  that.parent().removeClass('is-focus');
              }
          }).trigger('blur');
          $formInput.on('keyup keydown', function () {
              let that = $(this);
              if (that.val().length === 0) {
                  that.parent().removeClass('is-filled');
              } else {
                  that.parent().addClass('is-filled');
              }
          });

    const trigger = $('a[data-trigger="enlarge"], [data-trigger="enlarge"]');
    const close = $('.f-modal-close');
    const modal = $('.f-modal');
    const body = $('body');
    var a, b;
    var template;
    function openModal(e) {
        e.preventDefault();
        var $this = $(this);
        var image = $('<img alt="" src="' + $this.data('enlarge-src') + '" />');
        template = $('<div class="f-modal f-modal-enlarge">' +
            '<div class="f-modal-table">' +
            '<div class="f-modal-cell">' +
            '<div class="f-modal-content">' +
            '<div class="f-modal-close"></div>' +
            '</div>' +
            '</div>' +
            '</div>' +
            '</div>');
        template.appendTo(body);
        image.appendTo(template.find('.f-modal-content'));
        body.removeClass('f-modal-is-open');
        modal.removeClass('f-modal-open');
        modal.hide();
        clearTimeout(b);
        if (!body.hasClass('f-modal-is-open')) {
            template.show();
            a = setTimeout(function () {
                body.addClass('f-modal-is-open');
                template.addClass('f-modal-open');
            }, 50);
        }
    }
    function closeModal() {
        body.removeClass('f-modal-is-open');
        template.removeClass('f-modal-open');
        b = setTimeout(function () {
            template.hide();
            template.children().remove();
            template.remove();
        }, 400);
    }
    trigger.on('click', openModal);
    $(document).on('click', '.f-modal-close', closeModal);
}
